# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime
import logging
import os
import six
import json
from google.cloud import translate_v2 as translate
from googleapiclient.discovery import build
from flask import Flask, redirect, render_template, request
from flask import jsonify

from google.cloud import datastore
from google.cloud import storage
from google.cloud import vision


CLOUD_STORAGE_BUCKET = "vengala"

app = Flask(__name__)

def translation(text):

    # https://codelabs.developers.google.com/codelabs/cloud-translation-python3#0

    translate_client = translate.Client()     
    response = translate_client.translate(text, target_language= 'te')
    return response["translatedText"]

@app.route("/")
def homepage():
    # Create a Cloud Datastore client.
    datastore_client = datastore.Client()

    # Use the Cloud Datastore client to fetch information from Datastore about
    # each photo.
    query = datastore_client.query(kind="Faces")
    # image_entities = list(query.fetch())
    image_entities = list(fetch_times(1))

    # Return a Jinja2 HTML template and pass in image_entities as a parameter.
    return render_template("homepage.html", image_entities=image_entities)

def fetch_times(limit):

    datastore_client = datastore.Client()
    query = datastore_client.query(kind='Faces')
    query.order = ['-timestamp']

    times = query.fetch(limit=limit)

    return times

@app.route("/upload_photo", methods=["GET", "POST"])
def upload_photo():
    photo = request.files["file"]

    # Create a Cloud Storage client.
    storage_client = storage.Client()

    # Get the bucket that the file will be uploaded to.
    bucket = storage_client.get_bucket("vengala")

    # Create a new blob and upload the file's content.
    blob = bucket.blob(photo.filename)
    blob.upload_from_string(photo.read(), content_type=photo.content_type)

    # Make the blob publicly viewable.
    blob.make_public()

    # Create a Cloud Vision client.
    vision_client = vision.ImageAnnotatorClient()

    # Use the Cloud Vision client to detect a face for our image.
    source_uri = "gs://{}/{}".format(CLOUD_STORAGE_BUCKET, blob.name)
    image = vision.Image(source=vision.ImageSource(gcs_image_uri=source_uri))
    faces = vision_client.face_detection(image=image).face_annotations
    
   # Based on the image uploaded we detect the image using labels
    response = vision_client.web_detection(image=image)
    annotations = response.web_detection
    image_detection = annotations.web_entities
    image_detected = ""

    if len(image_detection) > 0:
        image_detected = image_detection[0].description
    else:
        image_detected = "Unknown"

    #translating the image detected into target langugae
    translation_of_image_detected = translation(image_detected)

    #detcet all logos
    response = vision_client.logo_detection(image=image)
    logos = response.logo_annotations
    logo = ""
    if len(logos) > 0:
        logo = logos[0].description
    else:
        logo = "No Logos Found"  
    

    #detect all labels
    response = vision_client.label_detection(image=image)
    labels = response.label_annotations
    label = ""
    if labels:
        if len(labels) < 5:
            for j in labels:
                label = label + ", " + j.description
        else:
            for i in range(5):
                label = label + ", " + labels[i].description
        label = label[1:]
    else:
        label = "No labels found"  
    
    
    # see if any texts of any languages are found in the image uploaded and if any texts are found they are passed in to google search 
    response = vision_client.text_detection(image=image)
    texts = response.text_annotations[:10]
    text_found = ""
    if texts:
        for text in texts:
            entity_annotation = text 
            description_string = str(entity_annotation.description)
            text_found = text_found + " " + text.description
    else:
        text_found = "No Text Found"
    
    #translating the text found into target langugae    
    translation_of_text = translation(text_found)

    #Landmark detection   
    response = vision_client.landmark_detection(image=image)
    landmarks = response.landmark_annotations
    landmark = ""
    if len(landmarks) > 0:
        for i in landmarks:
            landmark = landmark + ", " + i.description
        landmark = landmark[1:]
    else:
        landmark = "No Landmarks Found"

    
    #based on the uploaded image we will find the url of similar image
    response = vision_client.web_detection(image=image)
    annotations = response.web_detection

    url_of_similar_image = ""
    if annotations.visually_similar_images:
        url_of_similar_image = annotations.visually_similar_images[0].url
    else:
        url_of_similar_image = "unknown"

    # If a face is detected, save to Datastore the likelihood that the face
    # displays 'joy,' as determined by Google's Machine Learning algorithm.
    if len(faces) > 0:
        face = faces[0]

        # Convert the likelihood string.
        likelihoods = [
            "Unknown",
            "Very Unlikely",
            "Unlikely",
            "Possible",
            "Likely",
            "Very Likely",
        ]
        face_joy = likelihoods[face.joy_likelihood]
    else:
        face_joy = "Unknown"
    
    
    # Create a Cloud Datastore client.
    datastore_client = datastore.Client()

    # Fetch the current date / time.
    current_datetime = datetime.now()

    # The kind for the new entity.
    kind = "Faces"

    # The name/ID for the new entity.
    name = blob.name

    # Create the Cloud Datastore key for the new entity.
    key = datastore_client.key(kind, name)

    # Construct the new entity using the key. Set dictionary values for entity
    # keys blob_name, storage_public_url, timestamp, and joy.
    entity = datastore.Entity(key)
    entity["blob_name"] = blob.name
    entity["image_public_url"] = blob.public_url
    entity["timestamp"] = current_datetime
    entity["joy"] = face_joy
    entity['image_detected'] = image_detected
    entity['translated_image_detected'] = translation_of_image_detected
    entity['logo'] = logo  
    entity['label'] = label  
    entity['text'] = text_found
    entity['translated_text'] = translation_of_text
    entity['landmark_detected'] = landmark
    entity['image_url'] = url_of_similar_image

    # Save the new entity to Datastore.
    datastore_client.put(entity)

    # Redirect to the home page.
    return redirect("/")


@app.errorhandler(500)
def server_error(e):
    logging.exception("An error occurred during a request.")
    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            e
        ),
        500,
    )


if __name__ == "__main__":
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host="127.0.0.1", port=8080, debug=True)