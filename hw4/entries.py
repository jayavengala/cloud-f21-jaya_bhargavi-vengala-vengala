from flask import render_template
from flask.views import MethodView
import gbmodel

class Entries(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(department = row[0], coursenumber = row[1], quater = row[2], year = row[3], instructor = row[4], review = row[5] ) for row in model.select()]
        return render_template('entries.html', entries=entries)
