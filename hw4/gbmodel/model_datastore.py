from .Model import Model
from datetime import date
from google.cloud import datastore

def from_datastore(entity):
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['department'], entity['coursenumber'], entity['quater'], entity['year'], entity['instructor'], entity['review']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cloud-f21-jaya-vengala')

    def select(self):
        query = self.client.query(kind = 'Reviewhw')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def insert(self, department, coursenumber, quater, year, instructor, review):
        key = self.client.key('Reviewhw')
        rev = datastore.Entity(key)
        rev.update( {
            'department': department,
            'coursenumber' : coursenumber,
            'quater' : quater,
            'year' : year,
            'instructor' : instructor,
            'review' : review,
            })
        self.client.put(rev)
        return True

