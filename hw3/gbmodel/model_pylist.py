"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = []

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: department, coursenumber, quater, year, instructor, review 
        :return: List of lists
        """
        return self.guestentries

    def insert(self, department, coursenumber, quater, year, instructor, review):
        """
        Appends a new list of values representing new message into guestentries
        :param department: String
        :param coursenumber: Integer
        :param quater: String
        :param year: Integer
        :param instructor: String
        :param review: String
        :return: True
        """
        params = [department, course number, quater, year, instructor, review]
        self.guestentries.append(params)
        return True
