"""
A simple guestbook flask app.
Data is stored in a SQLite database that looks something like the following:

+------------+------------------+------------+----------------+
| Name       | Email            | signed_on  | message        |
+============+==================+============+----------------+
| John Doe   | jdoe@example.com | 2012-05-28 | Hello world    |
+------------+------------------+------------+----------------+

This can be created with the following SQL (see bottom of this file):

    create table guestbook (department text, coursenumber text, quater, year, instructor, review);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from guestbook")
        except sqlite3.OperationalError:
            cursor.execute("create table guestbook (department string, coursenumber int, quater string, year int, instructor string, review string)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: deaprtment, coursenumber, quater, year, instructor, review
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM guestbook")
        return cursor.fetchall()

    def insert(self, department, coursenumber, quater, year, instructor, review):
        """
        Inserts entry into database
        param department: String
        :param coursenumber: Integer
        :param quater: String
        :param year: Integer
        :param instructor: String
        :param review: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'department':department, 'coursenumber':coursenumber, 'quater':quater, 'year':year, 'instructor':instructor, 'review':review}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into guestbook (department, coursenumber, quater, year, instructor, review) VALUES (:department, :coursenumber, :quater, :year, :instructor, :review)", params)

        connection.commit()
        cursor.close()
        return True
