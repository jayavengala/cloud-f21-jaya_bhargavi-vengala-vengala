class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, department, coursenumber, quater, year, instructor, review):
        """
        Inserts entry into database
        :param department: String
        :param coursenumber: Integer
        :param quater: String
        :param year: Integer
        :param instructor: String
        :param review: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
